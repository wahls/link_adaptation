classdef la_static < la_baseclass
    % Static modulation and coding scheme. Implements la_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = la_static(NSUBCARRIERS,MODULATION,CODE_RATE,PACKET_LEN)
    %     
    %     NSUBCARRIERS: number of subcarriers
    %     MODULATION: modulation alphabet
    %     PACKET_LEN: packet length
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
        
    methods
        function obj = la_static(nsubcarriers,modulation,code_rate,...
                packet_len)
            obj.mod_table = modulation*ones(nsubcarriers,packet_len);
            obj.code_rate = code_rate;
            obj.packet_len = packet_len;
        end
        
        function update(obj,info)
        end
        
        function next_chan(obj)
        end
        
        function reset(obj)
        end
    end
end
