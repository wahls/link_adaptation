function icassp13_tune_predictors(stats)
% icassp13_tune_predictors(STATS)
%
%   Brute force search for good predictor parameters. Results are
%   saved to files. Requires Parallel Computing Toolbox.
%
%   STATS: generate with icassp13_generate_training_data

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% console output will be saved to a file for later reference
ds = datestr(now);
if ~isdir('results-icassp13')
    mkdir('results-icassp13');
end
file_name = sprintf('./results-icassp13/%s_tuning',ds);
diary([file_name,'.txt']);

%%% Start evaluation %%%

results = {};
names = {};
learning_curves = [];
for predictor={'NWM','QKLMS','KNN'};
    for feature={'Sorted SNR','Mean SNR'}
        names{end+1} = sprintf('%s %s',predictor{:},feature{:});
        fprintf('\nEvaluating %s:\n\n',names{end});
        [results{end+1},curve] = icassp13_tune_predictors_aux(stats,...
            predictor{:},feature{:});
        learning_curves = [learning_curves; aux_movavg(curve,125)];
        results{end}
    end
end
 
%%% Generate plot with learning curves %%%

font_size = 18;
line_width = 2;

fig1 = figure('Visible','Off');
h = plot(learning_curves');
set(h,'LineWidth',line_width);
set(gca,'FontSize',font_size,'LooseInset',get(gca,'TightInset'));
legend(names,'Location','SouthEast');
title('');
xlabel('Number of Transmitted Packets []');
ylabel('Ratio of Correct Classification []')
ylim([min(min(learning_curves)) max(max(learning_curves))]);
print(fig1,'-depsc',sprintf('%s.eps',file_name));

% stop writing console output to file
diary off;
fprintf('\nResults have been saved in the files ''%s.{txt,eps}''.\n',...
    file_name);
set(fig1,'Visible','On');
