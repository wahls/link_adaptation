classdef (Abstract) transceiver_baseclass < handle
    % Base class for multi-carrier transceivers. The class is abstract and
    % cannot be instantiated.
    %
    % METHODS
    %
    %   [TPT,PER] = transceive(CHANNEL,AMC,LA)
    %
    %     Transceives random data through a channel using link adaptation
    %     and adaptive modulation and coding.
    %
    %     CHANNEL: channel_baseclass object
    %     AMC: amc_baseclass object
    %     LA: la_baseclass objetc
    %     TPT: throughput [Mbit/s]
    %     PER: packet error rate

    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    methods
        [tpt,per] = transceive(obj,channel,amc,la)
    end
end