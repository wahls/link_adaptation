function [configs,params] = spawc13_configs(name,varargin)
% [CONFIGS,PARAMS] = spawc13_configs('default')
% [CONFIGS,PARAMS] = spawc13_configs('tune_predictors')
% [CONFIGS,PARAMS] = spawc13_configs('tune_criterion',ETA,RHO)
% [CONFIGS,PARAMS] = spawc13_configs('tune_thresholds',SNR,MOD,...
% CODE_RATE,PACKET_LEN)
% [CONFIGS,PARAMS] = spawc13_configs('simulation')
% [CONFIGS,PARAMS] = spawc13_configs('baseline')
%
%   Returns configurations for use with the spawc13_... functions. Bundled
%   here so there is a central place for the simulation parameters.
%
%   ETA: weight for the criterion of la_max_goodput2
%   RHO: exploration horizon for the criterion of la_max_goodput2
%   SNR: signal-to-noise ratio [dB]
%   CODE_RATE: code rate
%   PACKET_LEN: packet length
%   CONFIGS: cell array of structs for use with run_simulation(...)
%   PARAMS: struct containing the simulation parameters

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

params = struct();

% sample time of the channel [sec]
params.ts = 4e-6/52;

% max Doppler shift of the channel [Hz]
params.fd_max = 11.15;

% number of subcarriers
params.fft_size = 64;

% length of the cyclic prefix
params.cp_size = 12;

% worst-case path delay
params.max_delay = params.cp_size;

% maximum number of taps
params.max_ntaps = floor(params.max_delay*3/4);

% number of OFDM symbols per frame
params.nsymbols = 25;

% number of frames per channel
params.npackets = 100;

% [min max] average channel (pre-equalization) signal-to-noise ratio [dB]
params.snr = [0 30];

% SNR thresholds for inner link adaptation
params.thresholds = [0 5 13 19;4 9 16 23]';

% SNR threshold offsets [dB]
params.deltas = [-10:0.25:10];

% parameters for the QKLMS algorithm (predictor_qklms)
params.step_size = 0.2;
params.h = 0.1;
params.epsilon = 0.1;
params.code_book_size = 250;

% parameters for the criterion of la_max_goodput2
params.rho = 0.2;
params.eta = 0.2;

configs = {};
cnt = 1;
nsubcarriers = params.fft_size-params.cp_size;

% identical for all configs
transceiver = @()transceiver_bicmofdm(params.fft_size,...
    params.cp_size,params.npackets);
amc = @()amc_qamviterbi();
channel = @()channel_mtrayleigh(params.ts,params.fd_max,...
    params.max_ntaps,params.max_delay,'snr',params.snr);
feature = @()feature_delta_doppler('scale_delta',...
    1/max(abs(params.deltas)),'scale_doppler',1/params.fd_max);

switch name
    case 'tune_predictors'
        la = la_max_goodput2(nsubcarriers,feature(),{},...
            'mode','tuning','thresholds',params.thresholds,...
            'deltas',params.deltas,'stats',true);
        configs{1} = struct('name','Tune Predictors',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        
    case 'tune_criterion'
        predictors = cell(2,1);
        for cspl=1:2 % coding scheme/packet length
            predictors{cspl} = predictor_qklms(params.step_size,'kernel_type',...
                'gaussian','kernel_params',params.h,...
                'code_book_size',params.code_book_size,'epsilon',params.epsilon);
        end
        la = la_max_goodput2(nsubcarriers,feature(),predictors,...
            'mode','default','thresholds',params.thresholds,...
            'deltas',params.deltas,'stats',false,'eta',varargin{1},...
            'rho',varargin{2});
        configs{1} = struct('name','Tune Criterion',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        
    case 'tune_thresholds'
        chan = channel_awgn(varargin{1});
        la = la_static(nsubcarriers,varargin{2},varargin{3},varargin{4});
        configs{1} = struct('name','AWGN with fixed MCS',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',chan);
        
    case 'baseline'
        la = la_arf(nsubcarriers);
        configs{1} = struct('name','ARF',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());

    case 'simulation'
        la = la_arf(nsubcarriers);
        configs{1} = struct('name','ARF',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        predictors = cell(2,1);
        for cspl=1:2 % coding scheme/packet length
            predictors{cspl} = predictor_qklms(params.step_size,...
            'kernel_type','gaussian','kernel_params',params.h,...
            'code_book_size',params.code_book_size,'epsilon',params.epsilon);
        end
        la = la_max_goodput2(nsubcarriers,feature(),predictors,...
            'mode','default','thresholds',params.thresholds,...
            'deltas',params.deltas,'stats',false,'eta',params.eta,...
            'rho',params.rho);
        configs{2} = struct('name','Proposed',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        
    otherwise
        error('unknown name');
end
