classdef feature_meansnr < feature_baseclass
    % Mean signal-to-noise ratio. Implements feature_baseclass. The info
    % struct provide to extract is expected to have a field snr containing
    % a vector with the signal-to-noise ratios per subcarrier in linear
    % scale. The generated feature vector is a scalar containing the mean
    % snr, probably taken over the snrs in dB.
    %
    % CONSTRUCTORS
    %
    %   OBJ = feature_meansnr(ARG1,VAL1,...,ARGN,VALN)
    %
    %     Possible ARGS are:
    %     'DB': Should SNRs be converted to dB before the mean is
    %     taken? (bool, default true)
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        dB
    end
    methods
        function obj = feature_meansnr(positions,varargin)
            p = inputParser;
            p.addOptional('dB',true,@(x)isscalar(x)&&(x==true|x==false));
            p.parse(varargin{:});
            obj.dB = p.Results.dB;
        end
        function x = extract(obj,info)
            if ~obj.dB
                x = mean(info.snr);
            else
                x = mean(10*log10(info.snr));
            end
        end
    end
end