classdef amc_qamviterbi < amc_baseclass
    % Adaptive modulation and coding with QAM modulation, random
    % interleaving, convolutional coder and Viterbi decoder. Implements
    % amc_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = amc_qamviterbi()
    %
    %     No options.
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        henc 		% encoder handle
        hdec 		% decoder handle
        dec_delay 	% decision delay of the decoder
        hmod 		% cell array of modulator handles
        hdemod 		% cell array of demodulator handles
        scl 		% vector of average symbol powers per modulation alphabet
        bpm 		% vector of bits per modulation symbol
        stream 		% RandStream that generates the permutations
    end
    
    methods (Hidden)
        function [idx_iq,idx_bits] = analyse_mod_vector(obj,...
                mod_vector,mod,bits_per_symbol)
            % Finds all IQ symbols of a given modulation as well as
            % the positions of the corresponing bits.
            %
            % MOD_VECTOR: resized version of mod_table
            % MOD: modulation of interest
            % BITS_PER_SYMBOL: bits per IQ symbols (modulated with MOD)
            % IDX_IQ: indexes all IQ symbols with modulation MOD
            % IDX_BITS: indexes the corresponding positions in a BITS vector
            idx_iq = find(mod_vector==mod);
            idx_bits = zeros(bits_per_symbol*length(idx_iq),1);
            offsets = [0;cumsum(log2(mod_vector(1:(end-1))))];
            for i=1:bits_per_symbol
                e = zeros(bits_per_symbol,1);
                e(i) = 1;
                idx_bits = idx_bits+kron(offsets(idx_iq)+i,e);
            end
        end
    end
    
    methods
        function obj = amc_qamviterbi()
            % Simple constructor, no options.
            obj.henc = comm.ConvolutionalEncoder();
            obj.hdec = comm.ViterbiDecoder('InputFormat','Hard');
            obj.hmod = cell(4,1);
            obj.hdemod = cell(4,1);
            obj.dec_delay = obj.hdec.TracebackDepth*log2( ...
                obj.hdec.TrellisStructure.numInputSymbols);
            obj.scl = zeros(4,1);
            obj.bpm = [1 2 4 6];
            for i=1:4
                obj.hmod{i} = modem.qammod();
                obj.hmod{i}.inputtype = 'bit';
                obj.hmod{i}.M = 2^obj.bpm(i);
                obj.scl(i) = sqrt(mean(abs(obj.hmod{i}.constellation).^2));
                obj.hdemod{i} = modem.qamdemod();
                obj.hdemod{i}.outputtype = 'bit';
                obj.hdemod{i}.M = 2^obj.bpm(i);
            end
            obj.stream = RandStream('mt19937ar','Seed',0);
            obj.mods = [1 2 4 16 64];
            obj.code_rates = [0.5 0.75];
        end
        
        function idx = intrlv(obj,nbits)
            % Permutation of 1:NBITS.
            reset(obj.stream,0);
            idx = randperm(obj.stream,nbits);
        end
        
        function [nin,npad] = nbits_in(obj,mod_table,code_rate)
            [nsubs,nsymbols] = size(mod_table);
            max_nout = sum(sum(log2(mod_table)));
            nin = 3*floor(code_rate*max_nout/3);
            % nin being a multiple of 3 is a constraint of the encoder
            nout = round(nin/code_rate);
            npad = max_nout-nout;
        end
        
        function iq = encmod(obj,bits,mod_table,code_rate,npad)
            % catch "no transmission"
            nsymbols = prod(size(mod_table));
            if sum(sum(log2(mod_table)))==0
                iq = zeros(nsymbols,1);
                return;
            end
            
            % reset encoder
            release(obj.henc);
            reset(obj.henc);
            switch code_rate
                case 0.5
                    obj.henc.PuncturePatternSource = 'None';
                case 0.75
                    obj.henc.PuncturePatternSource = 'Property';
                otherwise
                    error('unsupported code rate');
            end
            
            % encode, pad, interleave
            bits = [step(obj.henc,bits);zeros(npad,1)];
            bits = bits(obj.intrlv(length(bits)));
            
            % modulate
            mod_vector = reshape(mod_table,nsymbols,1);
            iq = zeros(nsymbols,1);
            for i=1:4
                [idx_iq,idx_bits] = obj.analyse_mod_vector(mod_vector, ...
                    2^obj.bpm(i),obj.bpm(i));
                if length(idx_iq)>0
                    iq(idx_iq) = modulate(obj.hmod{i},bits(idx_bits)) ...
                        /obj.scl(i);
                end
            end
        end
        
        function bits = demdec(obj,iq,mod_table,code_rate,nout)
            % catch "no transmission"
            if sum(sum(log2(mod_table)))==0
                bits = [];
                return;
            end
            
            % demodulate
            nsymbols = length(iq);
            mod_vector = reshape(mod_table,nsymbols,1);
            nbits = sum(log2(mod_vector));
            bits = nan(nbits,1);
            for i=1:4
                [idx_iq,idx_bits] = obj.analyse_mod_vector(mod_vector, ...
                    2^obj.bpm(i),obj.bpm(i));
                if length(idx_iq)>0
                    bits(idx_bits) = demodulate(obj.hdemod{i}, ...
                        obj.scl(i)*iq(idx_iq));
                end
            end
            
            % deinterleave, depad
            [~,invintrlv] = sort(obj.intrlv(length(bits)));
            bits = bits(invintrlv');
            
            % reset decoder, decode
            release(obj.hdec);
            reset(obj.hdec);
            delay = ceil(obj.dec_delay/code_rate);
            switch code_rate
                case 0.5
                    obj.hdec.PuncturePatternSource = 'None';
                    npad = mod(6-mod(length(bits)+delay,6),6);
                case 0.75
                    obj.hdec.PuncturePatternSource = 'Property';
                    npad = mod(4-mod(length(bits)+delay,4),4);
                otherwise
                    error('unsupported code rate');
            end
            bits = step(obj.hdec,[bits;zeros(delay+npad,1)]);
            bits = bits((obj.dec_delay+1):(obj.dec_delay+nout));
        end
        
        function per = compare(obj,bits1,bits2,code_rate)
            delay = obj.dec_delay;
            per = sum(bits1(1:(end-delay))~=bits2(1:(end-delay)))>0;
        end
    end
end
