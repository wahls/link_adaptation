classdef (Abstract) amc_baseclass < handle
    % Base class for adaptive modulation and coding in multi-carrier
    % systems. SISO only, multiple antennas can be mapped to virtual
    % subcarriers. The class is abstract and cannot be instantiated. 
    % 
    % PROPERTIES (protected)
    %
    %   mods
    %
    %     vector containing the available modulation alphabets
    %
    %   code_rates
    %
    %     vector containing the available code rates
    %
    % METHODS
    %
    %   [NIN,NPAD] = nbits_in(MOD_TABLE,CODE_RATE)
    %
    %     Number of bits that can be encoded in a packet.
    %
    %     MOD_TABLE: (number of subcarriers)x(number of OFDM symbols)
    %     matrix with entries taken from obj.mods
    %     CODE_RATE: any entry of obj.code_rates
    %     NIN: number of bits that can be encoded
    %     NPAD: number of bits that the encoder will have to pad (use with
    %     encmod)
    %
    %   IQ = encmod(BITS,MOD_TABLE,CODE_RATE,NPAD);
    %
    %     Encodes and modulates a vector of bits.
    %
    %     BITS: vector containing NIN (use nbits_in) uncoded bits (0 or 1)
    %     MOD_TABLE: modulation per subcarrier and packet for encoded bits
    %     CODE_RATE: code rate used for encoding BITS
    %     NPAD: obtain from nbits_in(...)
    %     IQ: vector containing prod(size(MOD_TABLE)) IQ symbols
    %
    %   BITS = demdec(IQ,MOD_TABLE,CODE_RATE,NOUT)
    %
    %     Demodulates and decodes a vector of IQ symbols.
    %
    %     IQ: (number of subcarriers)x(number of packets) matrix of IQ data
    %     MOD_TABLE: modulation per subcarrier and packet of IQ
    %     CODE_RATE: code rate for decoding the demodulated bits
    %     NOUT: number of uncoded bits contained in IQ
    %     BITS: vector containing NOUT bits (0 or 1)
    %
    %   PER = compare(BITS1,BITS2,CODE_RATE)
    %
    %     Estimates the packet error rate. Crude estimates, like 1 if
    %     there is a packet error and 0 otherwise, will do.
    %
    %     BITS1,BITS2: Vectors of code bits (same length, 1 packet)
    %     CODE_RATE: code rate for decoding of BITS1,BITS2
    %     PER: estimate of the packet error rate
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (SetAccess=protected)
        mods
        code_rates
    end
    
    methods
        [nin,npad] = nbits_in(mod_table,code_rate)
        iq = encmod(obj,bits,mod_table,code_rate,npad)
        bits = demdec(obj,iq,mod_table,code_rate,nout)
        per = compare(obj,bits1,bits2,code_rate)
    end
end
