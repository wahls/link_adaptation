classdef la_arf < la_baseclass
    % Autorate Fallback. Implements la_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = la_arf(NSUBCARRIERS,ARG1,VAL1,...,ARGN,VALN)
    %
    %     NSUBCARRIERS: number of subcarriers
    %     Possible ARGS are
    %     'MCS_TABLE': available modulation and coding schemes, the format
    %     is [M_1 c_1 p_1;...;M_n c_n pl_n] where the i-th MCS uses
    %     modulation M_i, code rate c_i, and packet length pl_i
    %     'NMAX_SUCCESS': number of successive error-free packets after
    %     which the MCS is increased
    %     'NMAX_FAILURE': number of successive erroneous packets after
    %     which the MCS is decreased
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        nsubcarriers
        mcs             % index of current mcs -> mcs_table(mcs,:)
        mcs_table
        nmcs            % size(mcs_table,1)
        nsuccess        % the last nsuccess packets were error-free
        nfailure        % the last nfailure packets were erroneous
        nmax_success
        nmax_failure
    end
    
    methods (Hidden)
        function set_mcs(obj,mcs)
            obj.mcs = mcs;
            obj.mod_table = obj.mcs_table(mcs,1)*...
                ones(obj.nsubcarriers,obj.mcs_table(mcs,3));
            obj.code_rate = obj.mcs_table(mcs,2);
        end
    end   
    
    methods
        
        function obj = la_arf(nsubcarriers,varargin)
            obj.nsubcarriers = nsubcarriers;
            p = inputParser;
            p.addOptional('mcs_table',[2 0.5 25;4 0.5 25;4 0.75 25;...
                16 0.5 25; 16 0.75 25; 64 0.75 25],@(x)ismatrix(x)...
                &&size(x,1)>0&&size(x,2)==3&&sum(sum(~isfinite(x)))==0);
            p.addOptional('nmax_success',10,@(x)isnumeric(x)&...
                isscalar(x)&&isfinite(x)&&x>0&&x==round(x));
            p.addOptional('nmax_failure',2,@(x)isnumeric(x)&...
                isscalar(x)&&isfinite(x)&&x>0&&x==round(x));
            p.parse(varargin{:});
            obj.mcs_table = p.Results.mcs_table;
            obj.nmax_success = p.Results.nmax_success;
            obj.nmax_failure = p.Results.nmax_failure;
            obj.nmcs = size(obj.mcs_table,1);
            obj.set_mcs(obj.nmcs); % start with highest MCS for faster adaptation
            obj.nsuccess = 0;
            obj.nfailure = 0;
        end
               
        function update(obj,info)
            if info.packet_error
                obj.nfailure = obj.nfailure+1;
                obj.nsuccess = 0;
                if obj.nfailure>=obj.nmax_failure
                    obj.set_mcs(max(1,obj.mcs-1));
                    obj.nfailure = 0;
                end
            else
                obj.nsuccess = obj.nsuccess+1;
                obj.nfailure = 0;
                if obj.nsuccess>=obj.nmax_success
                    obj.set_mcs(min(obj.nmcs,obj.mcs+1));
                    obj.nsuccess = 0;
                end
            end
        end

        function next_chan(obj)
            obj.reset();
        end
        
        function stat = stats(obj)
            stat = {};
        end
        
        function reset(obj)
            obj.nsuccess = 0;
            obj.nfailure = 0;
            obj.set_mcs(obj.nmcs); % start with highest MCS for faster adaptation
        end
    end
end