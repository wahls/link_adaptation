function y = aux_movavg(x,r)
% Y = aux_movavg(X,R)
%   Moving average with adaptive windows at the borders.
%
%   X: vector
%   R: 2*R+1 is the window size.
%   Y: vector, same size as X

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

y = zeros(size(x));
for i=1:length(x)
    idx = max(1,i-r):min(i+r,length(x));
    y(i) = mean(x(idx));
end
