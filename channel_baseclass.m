classdef (Abstract) channel_baseclass < handle
    % Base class for communication channels. The class is abstract and
    % cannot be instantiated.
    %
    % PROPERTIES (protected)
    %
    %   ts
    %
    %     sample time [secs]
    %
    %   fd
    %
    %     Doppler spread [Hz]
    %
    %   snr
    %
    %     average signal-to-noise ratio [dB]
    %   
    %   ntx
    %
    %     number of transmitting antennas
    %
    %   nrx
    %
    %     number of receiving antannas
    %
    % METHODS
    %
    %   IR = impulse_response()
    %
    %     Current impulse response of the channel.
    %     
    %     IR: nrx x N*ntx matrix
    %
    %   Y = filter(X)
    %
    %     Transmit the IQ vectors.
    %
    %     X: ntx x N matrix, average power per tx antenna should be one
    %     Y: nrx x N matrix
    %
    %   reset()
    %
    %     Reset channel, choose new parameters.
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (SetAccess=protected)
        ts
        fd
        snr
        ntx = 1
        nrx = 1
    end
    
    methods
        ir = impulse_response(obj);
        y = filter(obj,x);
        reset(obj);
    end
end
