classdef feature_sortedsnr < feature_baseclass
    % Subsampled ordered signal-to-noise ratio vector. The info
    % struct provided to extract is expected to have a field snr containing
    % a vector with the signal-to-noise ratios per subcarrier in linear
    % scale. The generated feature vector is a subsampled version of the
    % the sorted SNR vector, with SNRs probably converted to dB and
    % normalized by the number of specified subcarriers.
    %
    % CONSTRUCTORS
    %
    %   OBJ = feature_sortedsnr(POSITIONS,ARG1,VAL1,...,ARGN,VALN)
    %
    %     POSITIONS: vector of subcarrier indices ("sample points")
    %     Possible ARGS are:
    %     'DB': Convert SNRs into dB? (default true)
    %     'NORMALIZE': Scale feature vector by 1/length(POSITONS)? (default
    %     true)
    %     'SORT_ORDER': ascend' or 'descend' (default 'ascend')
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        positions
        dB
        normalize
        sort_order
    end
    
    methods
        function obj = feature_sortedsnr(positions,varargin)
            if ~isnumeric(positions)||~isvector(positions)...
                    ||sum(positions~=round(positions)|positions<1|...
                    positions==inf)>0
                error('positions is not a vector with natural entries');
            end
            p = inputParser;
            p.addOptional('dB',true,@(x)isscalar(x)&&(x==true|x==false));
            p.addOptional('normalize',true,@(x)isscalar(x)...
                &&(x==true|x==false));
            p.addOptional('sort_order','ascend',@(x)isstring(x)...
                &&(strcmp(x,'ascend')|strcmp(x,'descend')));
            p.parse(varargin{:});
            obj.positions = positions;
            obj.dB = p.Results.dB;
            obj.normalize = p.Results.normalize;
            obj.sort_order = p.Results.sort_order;
        end
        
        function x = extract(obj,info)
            tmp = sort(info.snr,obj.sort_order);
            x = tmp(obj.positions);
            if obj.dB
                x = 10*log10(x);
            end
            if obj.normalize
                x = x/length(x);
            end
        end
    end
end