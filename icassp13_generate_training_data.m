function icassp13_generate_training_data(nruns,nchan)
% icassp13_generate_training_data(NRUNS,NCHANS)
%
%   Generates training data that can be used as input for the
%   icassp13_tune_... functions. Results are saved to files. Requires
%   Parallel Computing Toolbox.
%
%   NRUNS: number of simulation runs to perform
%   NCHAN: number of channels per simulation run.

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% console output will be saved to a file for later reference
ds = datestr(now);
if ~isdir('results-icassp13')
    mkdir('results-icassp13');
end
file_name = sprintf('./results-icassp13/%s_train',ds);
diary([file_name,'.txt']);

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%   small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

% switch to parfor-compatible random number generator
stream = RandStream('mrg32k3a');
RandStream.setGlobalStream(stream);

% show configuration details
[configs,params] = icassp13_configs('generate_training_data');
nruns
nchan
params
salt = 0

%%% Run nruns simulations %%%

stats = {};
for k=1:ceil(nruns/parfor_max)
    parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
        fprintf('[%s] simulation %i has been started\n',datestr(now),i);
        set(stream,'Substream',i);
        [~,~,s] = run_simulation(salt+i,nchan,configs);
        stats = [stats;s];
        fprintf('[%s] simulation %i has been completed\n',datestr(now),i);
    end
    diary off; % flush diary
    diary on;
end

%%% Save results %%%

save(sprintf('%s.mat',file_name),'stats');
fprintf('\nsaved log and results in the files %s.{txt,mat}\n',file_name);
diary off;
