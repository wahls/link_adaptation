function [results,best_learning_curve] = icassp13_tune_predictors_aux(...
    stats,predictor,feature)
% [RESULTS,LEARNING_CURVE] = icassp13_tune_predictors(STATS,PREDICTOR,...
% FEATURE)
%
%   Brute force search for good predictor parameters. Results are
%   saved to files. Requires Parallel Computing Toolbox.
%
%   STATS: generate with icassp13_generate_training_data
%   PREDICTOR: 'NWM', 'QKLMS' or 'KNN'
%   FEATURE: 'Sorted SNR' or 'Mean SNR'
%   RESULTS: struct containing the best found parameters
%   LEARNING_CURVE: ratio of correct classification over packet no.

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%   small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

[configs,params] = icassp13_configs('tuning');

switch feature
    case 'Sorted SNR'
        la = configs{1}.la;
    case 'Mean SNR'
        la = configs{2}.la;
    otherwise
        error('unknown feature');
end

ds = [0.95 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1]; % Deltas for NWM
hs = [0.1 0.25 0.5 0.75 1 2 3 4 5 6]; % Kernel bandwidths
mus = [0.2 0.5]; % step sizes for QKLMS
es = [0.01 0.05 0.1 0.25 0.5 0.75 1 2 5 10]; % epsilons for QKLMS
ks = [1 3 5 10 15 25]; % number of neighbors for KNN
rs = [0.01 0.05 0.1 0.5 1 5 10 50 100 500 1000]; % rhos for KNN

results = struct();
results.perf = -inf;
results.predictor = predictor;
results.feature = feature;
switch predictor
    case 'NWM'
        results.d = nan;
        results.h = nan;
        for h=hs
            for d=ds
                fprintf('[%s] h=%g, d=%g ...',datestr(now),h,d);
                drawnow;
                learning_curve = [];
                for k=1:ceil(length(stats)/parfor_max)
                    for i=((k-1)*parfor_max+1):min(k*parfor_max,...
                            length(stats))
                        predictors = {};
                        for j=1:6
                            predictors{j} = predictor_nwm(h,d,...
                                params.code_book_size);
                        end
                        learning_curve = [learning_curve;...
                            la.test_predictors(stats{i},predictors)];
                    end
                end
                learning_curve = sum(learning_curve,1)/length(stats);
                perf = mean(learning_curve(end-500+1:end));
                if perf>results.perf
                    results.perf = perf;
                    results.h = h;
                    results.d = d;
                    best_learning_curve = learning_curve;
                end
                fprintf(' perf=%g\n',perf);
            end
        end
        
    case 'QKLMS'
        results.mu = nan;
        results.h = nan;
        results.e = nan;
        for mu=mus
            for h=hs
                for e=es
                    fprintf('[%s] mu=%g, h=%g, epsilon=%g ...',...
                        datestr(now),mu,h,e);
                    drawnow;
                    learning_curve = [];
                    for k=1:ceil(length(stats)/parfor_max)
                        for i=((k-1)*parfor_max+1):min(k*parfor_max,...
                                length(stats))
                            predictors = {};
                            for j=1:6
                                predictors{j} = predictor_qklms(mu,...
                                    'code_book_size',...
                                    params.code_book_size,'kernel_type',...
                                    'gaussian','kernel_params',h,...
                                    'epsilon',e);
                            end
                            learning_curve = [learning_curve;...
                                la.test_predictors(stats{i},predictors)];
                        end
                    end
                    learning_curve = sum(learning_curve,1)/length(stats);
                    perf = mean(learning_curve(end-500+1:end));
                    if perf>results.perf
                        results.perf = perf;
                        results.mu = mu;
                        results.h = h;
                        results.e = e;
                        best_learning_curve = learning_curve;
                    end
                    fprintf(' perf=%g\n',perf);
                end
            end
        end

    case 'KNN'
        results.k = nan;
        results.r = nan;
        for kk=ks
            for r=rs
                    fprintf('[%s] k=%g, r=%g ...',datestr(now),kk,r);
                    drawnow;
                    learning_curve = [];
                    for k=1:ceil(length(stats)/parfor_max)
                        for i=((k-1)*parfor_max+1):min(k*parfor_max,...
                                length(stats))
                            predictors = {};
                            for j=1:6
                                predictors{j} = predictor_knn(kk,r,...
                                    params.code_book_size);
                            end
                            learning_curve = [learning_curve;...
                                la.test_predictors(stats{i},predictors)];
                        end
                    end
                    learning_curve = sum(learning_curve,1)/length(stats);
                    perf = mean(learning_curve(end-500+1:end));
                    if perf>results.perf
                        results.perf = perf;
                        results.k = kk;
                        results.r = r;
                        best_learning_curve = learning_curve;
                    end
                    fprintf(' perf=%g\n',perf);
            end
        end
        
    otherwise
        error('unknown predictor');
end

