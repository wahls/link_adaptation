classdef feature_delta_doppler < feature_baseclass
    % SNR threshold and Doppler. Implements feature_baseclass. The info
    % structs passed to extract() are expected to provide a delta and a
    % doppler_shift field. The generated feature vector is
    % x=[obj.scale_delta*info.delta;obj.scale_doppler*info.doppler_shift].
    %
    % CONSTRUCTORS
    %
    %   OBJ = feature_delta_doppler(ARG1,VAL1,...,ARGN,VALN)
    %
    %     Possible ARGS are
    %     'SCALE_DELTA': Scaling factor for thresholds (default 1)
    %     'SCALE_DOPPLER': Scaling factor for Doppler shifts (default 1)
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        scale_delta
        scale_doppler
    end
    methods
        function obj = feature_delta_doppler(varargin)
            p = inputParser;
            p.addOptional('scale_delta',1,@(x)isscalar(x)&&x>0&&x<inf);
            p.addOptional('scale_doppler',1,@(x)isscalar(x)&&x>0&&x<inf);
            p.parse(varargin{:});
            obj.scale_delta = p.Results.scale_delta;
            obj.scale_doppler = p.Results.scale_doppler;
        end
        function x = extract(obj,info)
            x = [obj.scale_delta*info.delta;...
                obj.scale_doppler*info.doppler_shift];
        end
    end
end