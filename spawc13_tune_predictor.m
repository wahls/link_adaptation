function spawc13_tune_predictor(kernel_type,nruns,nchan)
% spawc13_tune_predictor(KERNEL_TYPE,NRUNS,NCHAN)
%
%   Brute force search for good predictor (QKLMS) parameters. Results are
%   saved to files. Requires Parallel Computing Toolbox.
%
%   KERNEL_TYPE: 'gaussian','approx_gaussian','rational_quadratic'
%   NRUNS: number of simulation runs to perform
%   NCHAN: number of channels per simulation run.

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% console output will be saved to a file for later reference
ds = datestr(now);
if ~isdir('results-spawc13')
    mkdir('results-spawc13');
end
file_name = sprintf('./results-spawc13/%s_tuning',ds);
diary([file_name,'.txt']);

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%   small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

switch kernel_type
    case 'gaussian'
        kernel_params = @(h)h;
    case 'approx_gaussian'
        kernel_params = @(h)[h 2];
    case 'rational_quadratic'
        kernel_params = @(h)[h 2];
    otherwise
        error('unknown kernel_type');
end

% show configuration details
[configs,params] = spawc13_configs('tune_predictors');
fprintf('\nSetup:\n');
nruns
nchan
kernel_type
params
fprintf('\n');

%%% Collect training data %%%

fprintf(['\nWill run %i simulations to gather training data.\n\n'],nruns);
stats = {}; % stats will become a nruns x 1 x npackets cell array of
% structs with the fields "info", "cspl", "thresholds"
stream = RandStream('mrg32k3a'); % switch to parfor-compatible random
%    number generator
RandStream.setGlobalStream(stream);
for k=1:ceil(nruns/parfor_max)
    parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
        fprintf('[%s] simulation %i has been started\n',datestr(now),i);
        set(stream,'Substream',i);
        [~,~,s] = run_simulation(i,nchan,configs);
        stats{i} = s;
        fprintf('[%s] simulation %i has been completed\n',datestr(now),i);
    end
    diary off; % flush diary
    diary on;
end

%%% Select kernel bandwidth h %%%

fprintf('\n\nWill evaluate prediction performance for various\n');
fprintf('kernel bandwidths h and unconstrained code books.\n\n');
correct_classification_ratio = [];
best_h = -1;
best_ratio = -1;
N = length(stats{1}{1}); % total number of packets per simulation run
permutation = randperm(N);
rng(0);
cnt = 1; % count number of already tested h's, used to create file names
for h=10.^(0.1*(-30:2:30));
    fprintf('[%s] evaluating h=%g ... ',datestr(now),h);
    ncorrect_classifications = zeros(1,N);
    nbaseline_correct = zeros(1,N);
    for k=1:ceil(nruns/parfor_max)
        parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
            predictors = cell(2,1);
            for cspl=1:2 % coding scheme/packet length
                predictors{cspl} = predictor_qklms(params.step_size,...
		    'kernel_type',...
                    kernel_type,'kernel_params',kernel_params(h),...
                    'code_book_size',params.npackets*nchan);
            end
            ncorrect_classifications = ncorrect_classifications+...
                configs{1}.la.test_predictors(stats{i}{1}(permutation),predictors);
        end
    end
    correct_classification_ratio = [correct_classification_ratio;...
        ncorrect_classifications/nruns];
    ratio = mean(ncorrect_classifications(floor(N/2):end)/nruns);
    fprintf('ratio %g\n',ratio);
    if ratio>best_ratio
        best_ratio = ratio;
        best_h = h;
    end
    % save learning curve for this h
    fig = figure('Visible','Off');
    plot(1:N,aux_movavg(correct_classification_ratio(end,:),125));
    title(sprintf(['Learning curve for h=%g (%s kernel,',...
        ' unconstrained code book)'],h,kernel_type));
    print(fig,'-depsc',sprintf('%s_%.4i_h%g.eps',file_name,cnt,h));
    close(fig);
    diary off; % flush diary
    diary on;
    cnt = cnt+1;
end
fprintf('\n ==> best_h=%g, best_ratio=%g\n\n',best_h,best_ratio);

%%% Investigate losses of quantization %%%

fprintf('\nWill evaluate prediction performance for various\n');
fprintf('code books sizes and quantization thresholds.\n\n');
correct_classification_ratio = [];
labels = {}; % will become a cell array of strings describing the predictor
% configuration of the corresponding ratio
for code_book_size=[100 250 500 750 1000]
    for epsilon=10.^(0.1*(-30:2:30)); % quantization constant for QKLMS
        fprintf('[%s] evaluating code_book_size=%g and epsilon=%g ... ',...
            datestr(now),code_book_size,epsilon);
        ncorrect_classifications = zeros(1,N);
        for k=1:ceil(nruns/parfor_max)
            parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
                predictors = cell(2,1);
                for cspl=1:2 % coding scheme/packet length
                    predictors{cspl} = predictor_qklms(params.step_size,'kernel_type',...
                        kernel_type,'kernel_params',kernel_params(best_h),...
                        'code_book_size',code_book_size,...
                        'epsilon',epsilon);
                end
                ncorrect_classifications = ncorrect_classifications+...
                    configs{1}.la.test_predictors(stats{i}{1}(permutation),predictors);
            end
        end
        ratio = mean(ncorrect_classifications/nruns);
        labels{end+1} = sprintf('code_book_size=%g, epsilon=%g',...
            code_book_size,epsilon);
        correct_classification_ratio(end+1) = ratio;
        fprintf('ratio %g\n',correct_classification_ratio(end));
	diary off; % flush diary
	diary on;
    end
end
fprintf('\nResults, ordered by ratio:\n\n');
[~,ind] = sort(correct_classification_ratio,'ascend');
for i=ind
    fprintf('%g\t ratio for %s\n',correct_classification_ratio(i),labels{i});
end
fprintf('\n  (best ratio with unconstrained code book was %g)\n',best_ratio);

% stop writing console output to file
diary off;
fprintf('\nResults have been saved in the files ''%s.{txt,eps}''.\n',...
    file_name);
