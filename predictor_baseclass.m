classdef (Abstract) predictor_baseclass < handle 
    % Base class for non-parametric predictors. The class is abstract and
    % cannot be instantiated. 
    %
    % METHODS
    %
    %   update(X,Y)
    %
    %     Updates database after the outcome Y has been observed for the
    %     feature X.
    %
    %     X: column vector
    %     Y: scalar
    %
    %   Y = predict(X)
    %
    %     Predict the outcome Y from the feature X.
    %
    %     X: column vector
    %     Y: scalar
    %
    %   reset()
    %
    %     Reset internal variables to initial state.

    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    methods
        update(obj,x,y);
        y = predict(obj,x);
        reset(obj);
    end
end