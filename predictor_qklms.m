classdef predictor_qklms < predictor_baseclass
    % Quantized kernel least mean squares algorithm. Implements
    % predictor_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = predictor_qklms(MU,ARG1,VAL1,...,ARGN,VALN)
    %
    %     MU: Step size
    %     Possible ARGS are:
    %     'EPSILON': quantization threshold
    %     'CODE_BOOK_SIZE': upper bound on the code book size, with finite
    %     values memory will be preallocated (default inf)
    %     'KERNEL_TYPE': 'gaussian','sinc','polynomial','approx_gaussian',
    %     'rational_quadratic'
    %     'KERNEL_PARAMS': a bandwidth for the first two types, [bandwidth,
    %     degree] otherwise
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        X                           % code book of observed features,
                                    % X=[x1 x2 ... xn]
        al                          % weights of the code book entries
        mu
        epsilon
        code_book_size
        ncode_book_entries          % number of current entries (code book
                                    % may be bigger because of memory
                                    % preallocation)
        oldest_code_book_entry      % index of the oldest entry
        kernel                      % function pointer to kernel function
        kernel_params
    end
    
    methods (Hidden)
        % obj.kernel will point to one of the following routines. Each of
        % them evaluates the kernel for many code book entries U=[u1...un]
        % and one feature v
        function k = gaussian_kernel(obj,U,v)
            k = transp(exp(-0.5*sum(abs(U-v(:,ones(1,size(U,2)))).^2,1)...
                /obj.kernel_params^2));
        end
        function k = sinc_kernel(obj,U,v)
            k = transp(sinc(obj.kernel_params*(U-v(:,ones(1,size(U,2))))));
        end
        function k = polynomial_kernel(obj,U,v)
            h = obj.kernel_params(1);
            d = obj.kernel_params(2);
            k = (h+U'*v).^d;
        end
        function k = approx_gaussian_kernel(obj,U,v)
            h = obj.kernel_params(1);
            d = obj.kernel_params(2);
            k = transp(1./...
                (1+0.5*sum(abs(U-v(:,ones(1,size(U,2)))).^2,1)/h^2/d).^d);
        end
        function k = rational_quadratic_kernel(obj,U,v)
            h = obj.kernel_params(1);
            d = obj.kernel_params(2);
            tmp = 0.5*sum(abs(U-v(:,ones(1,size(U,2)))).^2,1);
            k = transp(1-tmp.^d./(h+tmp.^d));
        end
        % common routines for checking if obj.kernel_params is valid
        function kernel_params_check1(obj)
            if ~isnumeric(obj.kernel_params)...
                    |~isscalar(obj.kernel_params)...
                    |(obj.kernel_params<=0)...
                    |(obj.kernel_params==inf)...
                    error('kernel_params is not a finite positive scalar');
            end
        end
        function kernel_params_check2(obj)
            if ~isnumeric(obj.kernel_params)...
                    |~isvector(obj.kernel_params)...
                    |(length(obj.kernel_params)~=2)...
                    error('kernel_params is not a numeric vector of length 2');
            end
            if ~isfinite(obj.kernel_params(1))
                error('kernel_params(1) is not a finite scalar');
            end
            if obj.kernel_params(2)<=0|obj.kernel_params==inf
                error('kernel_params(2) is not a finite positive scalar');
            end
        end
    end
    
    methods
        function obj = predictor_qklms(mu,varargin)
            obj.X = [];
            obj.al = [];
            obj.mu = mu;
            p = inputParser;
            p.addOptional('epsilon',0,@(x)isnumeric(x)&&isscalar(x)...
                &&(x>=0));
            p.addOptional('code_book_size',inf,@(x)isnumeric(x)...
                &&isscalar(x)&&(x>=1));
            p.addOptional('kernel_type','gaussian',@(x)ischar(x)...
                &&isvector(x)&&(strcmp(x,'gaussian')...
                |strcmp(x,'polynomial')|strcmp(x,'sinc')...
                |strcmp(x,'approx_gaussian')|strcmp(x,'rational_quadratic')));
            p.addOptional('kernel_params',1);
            p.parse(varargin{:});
            obj.epsilon = p.Results.epsilon;
            obj.code_book_size = p.Results.code_book_size;
            obj.ncode_book_entries = 0;
            obj.oldest_code_book_entry = 1;
            obj.kernel_params = p.Results.kernel_params;
            switch p.Results.kernel_type
                case 'gaussian'
                    obj.kernel = @obj.gaussian_kernel;
                    obj.kernel_params_check1();
                case 'sinc'
                    obj.kernel = @obj.sinc_kernel;
                    obj.kernel_params_check1();
                case 'polynomial'
                    obj.kernel = @obj.polynomial_kernel;
                    obj.kernel_params_check2();
                case 'approx_gaussian'
                    obj.kernel = @obj.approx_gaussian_kernel;
                    obj.kernel_params_check2();
                case 'rational_quadratic'
                    obj.kernel = @obj.rational_quadratic_kernel;
                    obj.kernel_params_check2();
                otherwise
                    error('unknown kernel');
            end
        end
        
        function y = predict(obj,x)
            if obj.ncode_book_entries>0
                y = dot(obj.al(1:obj.ncode_book_entries),...
                    obj.kernel(obj.X(:,1:obj.ncode_book_entries),x));
            else
                y = 0;
            end
        end
        
        function update(obj,x,y)
            [m,n] = size(obj.X);
            if obj.ncode_book_entries>0 % non-empty code book
                tmp = sum(abs(obj.X-x(:,ones(1,n))),1)';
                e = y-obj.predict(x);
                [v,i] = min(tmp);
                if v<=obj.epsilon % feature is very close to a code word,
                    %    only update the weight
                    obj.al(i) = obj.al(i)+obj.mu*e/obj.kernel(x,x);
                elseif obj.ncode_book_entries<obj.code_book_size % feature
                    %    is sufficiently new, add to code book if not full
                    obj.ncode_book_entries = obj.ncode_book_entries+1;
                    obj.X(:,obj.ncode_book_entries) = x;
                    obj.al(obj.ncode_book_entries,1) = obj.mu*e/obj.kernel(x,x);
                else % feature is sufficiently new, replace with oldest
                    %    code book entry if full
                    obj.X(:,obj.oldest_code_book_entry) = x;
                    obj.al(obj.oldest_code_book_entry,1) = obj.mu*e/obj.kernel(x,x);
                    obj.oldest_code_book_entry = obj.oldest_code_book_entry+1;
                    if obj.oldest_code_book_entry>obj.code_book_size
                        obj.oldest_code_book_entry = 1;
                    end
                end
            else % empty code book
                if obj.code_book_size==inf % don't preallocated
                    obj.X = x;
                    obj.al = obj.mu*y;
                else % preallocate
                    obj.X = [x zeros(length(x),obj.code_book_size-1)];
                    obj.al = [obj.mu*y;zeros(obj.code_book_size-1,1)];
                end
                obj.ncode_book_entries = 1;
            end
        end
        
        function reset(obj)
            obj.X = [];
            obj.al = [];
            obj.ncode_book_entries = 0;
        end
    end
end
