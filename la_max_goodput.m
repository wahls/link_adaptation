classdef la_max_goodput < la_baseclass
    % Link adaptation aiming at goodput maximization. Implements
    % la_baseclass. There is a fixed set of modulation and coding schemes
    % (MCSs). The MCS with the best predicted performance is chosen.
    %
    % CONSTRUCTORS
    %
    %   OBJ = la_max_goodput(NSUBCARRIERS,FEATURE,PREDICTORS,ARG1,VAL1,
    %   ...,ARGN,VALN)
    %
    %     NSUBCARRIERS: the number of subcarriers
    %     FEATURE: feature_baseclass object
    %     PREDICTORS: cell array of predictor_baseclass objects (one
    %     per code rate)
    %     Possible ARGS are
    %     'MODS': modulation scheme of each MSC (default [2 4 4 16 16 64])
    %     'CODE_RATES': code rate of each MCS (default
    %     [0.5 0.5 0.75 0.5 0.75 0.75])
    %     'PACKET_LENS': packet length of each MCS (default
    %     [25 25 25 25 25 25])
    %     'MODE': 'default' or 'tuning'; 'tuning' selects the MCS randomly,
    %     use with 'STATS' to obtain training data for predictors (default
    %     'default')
    %     'STATS': keep statistics, calling obj.stats will return a cell
    %     array of structs that can be evaluated with test_predictors(...)
    %
    % METHODS
    %
    %   R = test_predictors(STATS,PREDICTORS)
    %
    %     Test prediction accuracy w.r.t. packet error rates.
    %
    %     STATS: output of obj.stats() after a simulation has been run
    %     in 'tuning' mode and with 'STATS'=true
    %     R: vector of length(stats), R(i)=1 if round(predicted PER)=
    %     stats{i}.info.packet_error and 0 otherwise 
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        feature             % feature_baseclass object
        predictors          % cell array of predictor_baseclass objects
        nsuccess            % counter for correct packets since last error
                            % (or reset) -> used for exploration
        nmax_success        % value after which nsucesses is reset and a
                            % higher than usually chosen MCS is explored
        nsubcarriers
        mods
        code_rates
        packet_lens
        nmcs                % number of mcs
        mcs                 % index of the currently used mcs
        mode
        keep_stats
        stat
    end
    
    methods (Hidden)
        function set_mcs(obj,mcs)
            obj.mcs = mcs;
            obj.mod_table = obj.mods(obj.mcs)*ones(obj.nsubcarriers, ...
                obj.packet_lens(obj.mcs));
            obj.code_rate = obj.code_rates(obj.mcs);
        end
    end
    
    methods
        function obj = la_max_goodput(nsubcarriers,feature,predictors,...
                varargin)
            obj.feature = feature;
            obj.predictors = predictors;
            obj.nsubcarriers = nsubcarriers;
            p = inputParser;
            p.addOptional('mods',[2 4 4 16 16 64],@(x)isnumeric(x)...
                &&isvector(x)&&sum(x<=0|x==inf)==0&&length(x)>1);
            p.addOptional('code_rates',[0.5 0.5 0.75 0.5 0.75 0.75],...
                @(x)isnumeric(x)&&isvector(x)&&sum(x<0|x>1)==0);
            p.addOptional('packet_lens',25*ones(1,6),@(x)isnumeric(x)...
                &&isvector(x)&&sum(x<1|x==inf)==0);
            p.addOptional('nmax_success',10,@(x)isnumeric(x)...
                &&isscalar(x)&&x>0&&x==round(x));
            p.addOptional('mode','default',@(x)strcmp(x,'default')...
                |strcmp(x,'tuning'));
            p.addOptional('stats',false,@(x)isscalar(x)&islogical(x));
            p.parse(varargin{:});
            obj.mods = p.Results.mods;
            obj.code_rates = p.Results.code_rates;
            obj.packet_lens = p.Results.packet_lens;
            obj.nmax_success = p.Results.nmax_success;
            obj.nsuccess = 0;
            obj.mode = p.Results.mode;
            obj.keep_stats = p.Results.stats;
            obj.nmcs = length(obj.mods);
            obj.stat = {};
            obj.set_mcs(obj.nmcs);
        end
        
        function update(obj,info)
            x = obj.feature.extract(info);
            y = info.packet_error;
            if obj.keep_stats
                obj.stat{end+1} = struct('mcs',obj.mcs,'info',info);
            end
            
            switch obj.mode
                case 'default'
                    % choose mcs with highest predicted goodput
                    best_mcs = 1;
                    best_gpt = -1;
                    for i=1:obj.nmcs
                        per = obj.predictors{i}.predict(x);
                        gpt = (1-per)*log2(obj.mods(i))*obj.code_rates(i);
                        if gpt>=best_gpt
                            best_gpt = gpt;
                            best_mcs = i;
                        end
                    end
                    
                    % update predictor with new info
                    obj.predictors{obj.mcs}.update(x,y);
                    
                    % explore next higher mcs if we had ten successive
                    % successful packets
                    if ~y
                        obj.nsuccess = obj.nsuccess+1;
                    else
                        obj.nsuccess = 0;
                    end
                    if obj.nsuccess>=obj.nmax_success
                        best_mcs = min(obj.mcs+1,obj.nmcs);
                        obj.nsuccess = 0;
                    end
                    
                case 'tuning'
                    best_mcs = randi(obj.nmcs);
                    
                otherwise
                    error('unknown mode');
            end
            
            obj.set_mcs(best_mcs);
        end
        
        function next_chan(obj)
            % no operation
        end
        
        function stat = stats(obj)
            stat = obj.stat;
        end
        
        function reset(obj)
            obj.nsuccess = 0;
            for i=1:length(obj.predictors)
                obj.predictors{i}.reset();
            end
            obj.stat = {};
        end

        function r = test_predictors(obj,stats,predictors)
            r = zeros(1,length(stats));
            for i=1:length(r)
                x = obj.feature.extract(stats{i}.info);
                y = stats{i}.info.packet_error;
                if isnan(y) % treat no transmission as packet error
                    y = 1;
                end
                per = predictors{stats{i}.mcs}.predict(x);
                r(i) = round(per)==y;
                predictors{stats{i}.mcs}.update(x,y);
            end
        end
    end
end