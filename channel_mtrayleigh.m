classdef channel_mtrayleigh < channel_baseclass
    % Normalized multi-tap Rayleigh channel with additive white Gaussian
    % noise and optional transceiver nonlinearity (a Rapp model).
    % Implements channel_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = channel_mtrayleigh(TS,FD_MAX,MAX_NTAPS,MAX_DELAY,ARG1,VAL1,
    %   ...,ARGN,VALN)
    %
    %     TS: sample time [sec]
    %     FD_MAX: maximum Doppler shift [Hz]
    %     MAX_NTAPS: maximum number of paths [samples]
    %     MAX_DELAY: maximum channel spread [samples]
    %     Possible ARGS are
    %     'SNR': Signal-to-noise ratios ([min_snr,max_snr], default [5 35])
    %     'BACKOFF': backoff of the Rapp model ([min_backoff,max_backoff],
    %     default [inf,inf] = linear amplifier)
    %     'SMOOTHNESS' smoothness of the Rapp model (default 2)
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        fd_max          % maximum Doppler spread [Hz]
        max_ntaps       % maximum number of taps []
        max_delay       % maximum delay per tap [number of samples]
        min_snr         % minimum signal to noise ratio [dB]
        max_snr         % maximum signal to noise ratio [dB]
        backoff         % current backoff of the TX amplifier [dB]
        min_backoff
        max_backoff
        smoothness      % smoothness of the TX amplifier []
        chan            % MATLAB channel object
    end
    
    methods
        function obj = channel_mtrayleigh(ts,fd_max,max_ntaps,...
                max_delay,varargin)
            obj.ts = ts;
            obj.fd_max = fd_max;
            obj.max_ntaps = max_ntaps;
            obj.max_delay = max_delay;
            p = inputParser;
            p.addOptional('snr',[5 35],@(x)isnumeric(x)&&isvector(x)...
                &&sum(isfinite(x))==2&&length(x)==2&&x(1)<=x(2));
            p.addOptional('backoff',[inf inf],@(x)isnumeric(x)...
                &&isvector(x)&&sum(isnan(x))==0&&length(x)==2&&x(1)<=x(2)...
                &&x(1)>-inf&&isinf(x(1))==isinf(x(2)));
            p.addOptional('smoothness',2,@(x)isnumeric(x)...
                &&isscalar(x)&&isfinite(x)&&x>0);
            p.parse(varargin{:});
            obj.min_snr = p.Results.snr(1);
            obj.max_snr = p.Results.snr(2);
            obj.min_backoff = p.Results.backoff(1);
            obj.max_backoff = p.Results.backoff(2);
            obj.smoothness = p.Results.smoothness;
            obj.reset();
        end
        
        function ir = impulse_response(obj)
            tau = 1+round(obj.chan.PathDelays/obj.chan.InputSamplePeriod);
            ir = zeros(max(tau),1);
            ir(tau) = transp(obj.chan.PathGains);
            ir = obj.tx_nonlinearity(ir);
        end
        
        function v = tx_nonlinearity(obj,u)
            % saturation level assuming input power one
            u_sat = 10^(0.1*obj.backoff);
            % apply Rapp model
            p = obj.smoothness;
            v = u./(1+(abs(u)/u_sat).^(2*p)).^(1/(2*p));
        end
        
        function y = filter(obj,x)
            % channel gains are normalized, on average total gain is one
            u = obj.chan.filter(obj.tx_nonlinearity(x));
            % assume power one for u
            y = u+sqrt(0.5/10^(0.1*obj.snr))...
                *(randn(size(u))+1i*randn(size(u)));
        end
        
        function reset(obj)
            obj.fd = rand()*obj.fd_max;
            ntaps = randi(max(1,obj.max_ntaps));
            tau = randperm(obj.max_delay)-1;
            tau = obj.ts*tau(1:ntaps);
            pdb = rand(1,ntaps);
            obj.chan = rayleighchan(obj.ts,obj.fd,tau,pdb);
            obj.chan.NormalizePathGains = 1;
            obj.chan.ResetBeforeFiltering = 0;
            obj.snr = obj.min_snr+rand()*(obj.max_snr-obj.min_snr);
            if obj.min_backoff~=inf
                obj.backoff = obj.min_backoff...
                    +rand()*(obj.max_backoff-obj.min_backoff);
            else
                obj.backoff = inf; % no nonlinearities
            end
        end
    end
end
