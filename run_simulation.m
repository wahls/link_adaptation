function [gpts,pers,stats] = run_simulation(seed,nchan,configs)
% [GPTS,PERS,STATS] = run_simulation(SEED,NCHAN,CONFIGS)
%
%   Performs a simulation.
%
%   SEED: for initialization of the random number generator
%   NCHAN: number of channels to simulate
%   CONFIGS: cell array of structs with fields 'name' (string),
%   'transceiver' (transceiver_baseclass object), 'channel'
%   (channel_baseclass object), 'amc' (amc_baseclass object), 'la'
%   (la_baseclass object)
%   GPTS: nchan x (number of configs) matrix of goodputs
%   PERS: nchan x (number of configs) matrix of packet error rates, NaN's
%   indicate that no packet has been send
%   STATS: (number of configs) x 1 cell array of outputs of la.stats(...)

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% check inputs
if nargin~=3
    error('run_simulation takes exactly three inputs');
end
if ~isscalar(seed)||~isfinite(seed)||seed~=round(seed)||seed<0
    error('nchan is not a natural number');
end
if ~isscalar(nchan)||~isfinite(nchan)||nchan~=round(nchan)||nchan<1
    error('nchan is not a positive natural number');
end
if ~iscell(configs)
    error('configs is not a cell array');
end

% initialize output variables
nconf = length(configs);
gpts = zeros(nchan,nconf);
pers = zeros(nchan,nconf);
stats = cell(nconf,1);

% iterate over configurations
for n=1:nconf    
    % extract name, tranceiver, channel adaptive modulation and coding,
    % and link adaptation for this configuration
    if ~isstruct(configs{n})
        error(sprintf('configs{%i} is not a struct',n));
    end
    field_names = {'name','transceiver','channel','amc','la'};
    field_types = {'char','transceiver_baseclass','channel_baseclass', ...
        'amc_baseclass','la_baseclass'};
    for i=1:5
        if ~isfield(configs{n},field_names{i})
            error(sprintf('configs{%i} has no field "%s"',n,field_names{i}));
        end
        if ~isa(configs{n}.(field_names{i}),field_types{i})
            error(sprintf('configs{%i}.%s is not a "%s" object', ...
                n,field_names{i},field_types{i}));
        end
    end
    name = configs{n}.name;
    channel = configs{n}.channel;
    transceiver = configs{n}.transceiver;
    amc = configs{n}.amc;
    la = configs{n}.la;
    la.reset();
    
    % initialize random number generator
    rng(seed);
    
    % iterate over channels
    for nc=1:nchan      
        % reset the channel
        channel.reset();

        % inform link adaptation that channel statistics have changed
        la.next_chan();
        
        % run simulation for this channel
        [tpt,per] = transceiver.transceive(channel,amc,la);
        
        % compute achieved goodput and packet error rate, only consider
        % time slots where actually data has been transmitted (per~=nan)
        idx = ~isnan(per);
        if length(idx)>0
            gpts(nc,n) = mean(tpt(idx).*(1-per(idx)));
            pers(nc,n) = mean(per(idx));
        end
    end
    
    % extract link adaptation statistics
    stat = la.stats();
    stats{n} = stat;
end
