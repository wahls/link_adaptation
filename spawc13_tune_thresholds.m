function spawc13_tune_thresholds(nruns)
% spawc13_tune_thresholds(NRUNS)
%
%   Evaluate goodput over SNR in AWGN channels. Used to find thresholds.
%   Results are saved to files. Requires Parallel Computing Toolbox.
%
%   NRUNS: number of simulation runs to perform

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% create directory for the plots
ds = datestr(now);
if ~isdir('results-spawc13')
    mkdir('results-spawc13');
end
file_name = sprintf('./results-spawc13/%s_awgn',ds);
packet_len = 25;
SNRs = 0:0.5:30;
nchan = 1; % does not matter in this case

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%   small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

% switch to parfor-compatible random number generator
stream = RandStream('mrg32k3a');
RandStream.setGlobalStream(stream);

%%% Run simulations %%%

y = [];
labels = {};
cnt = 1;
for code_rate=[0.5 0.75]
    for mod=[2 4 16 64]
        snr_index = 1;
        for snr=SNRs
            fprintf('[%s] evaluating code_rate=%g, mod=%g, snr=%g ... ',...
                datestr(now),code_rate,mod,snr);
            configs = spawc13_configs('tune_thresholds',snr,mod,...
                code_rate,packet_len);
            gpts = zeros(nruns,1);
            for k=1:ceil(nruns/parfor_max)
                for i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
                    set(stream,'Substream',i);
                    [g,~,~] = run_simulation(i,nchan,configs);
                    gpts(i) = mean(g(~isnan(g)));
                end
            end
            y(cnt,snr_index) = mean(gpts);
            snr_index = snr_index+1;
            fprintf('\n');
        end
        labels{cnt} = sprintf('R=%g,M=%g',code_rate,mod);
        cnt = cnt+1;
    end
end

%%% Generate and save plots %%%

font_size = 18;
line_width = 2;

fig = figure('Visible','Off');
plot(SNRs,y,'LineWidth',line_width);
set(gca,'FontSize',font_size,'LooseInset',get(gca,'TightInset'));
set(gca,'XMinorGrid','on');
legend(labels,'Location','NorthWest');
xlabel('SNR [dB]');
ylabel('Goodput [Bits/Time Unit]');
print(fig,'-depsc',sprintf('%s.eps',file_name));
close(fig);
fprintf('\nResults have been saved in the file ''%s.eps''.\n',...
    file_name);