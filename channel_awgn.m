classdef channel_awgn < channel_baseclass
    % Add white Gaussian noise. Implements channel_baseclass. The sample
    % time is set to 1e-6 sec. The Doppler shift is zero Hz.
    %
    % CONSTRUCTORS
    %
    %   OBJ = channel_awgn(SNR)
    %
    %     SNR: signal-to-noise ratio [dB]
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    methods
        function obj = channel_awgn(snr)
            if ~isnumeric(snr)|~isscalar(snr)|~isfinite(snr)|~isreal(snr)
                error('snr is not a real number');
            end
            obj.fd = 0; % channel is not time-varying
            obj.ts = 1e-6; % this value nicely cancels with the M in Mbit/s
            obj.snr = snr;
        end
        
        function ir = impulse_response(obj)
            ir = 1;
        end
        
        function y = filter(obj,x)
            % assume power one for x
            y = x+sqrt(0.5/10^(0.1*obj.snr))...
                *(randn(size(x))+1i*randn(size(x)));
        end
        
        function reset(obj)
            % No operation.
        end
    end
end
