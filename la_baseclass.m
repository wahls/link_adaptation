classdef (Abstract) la_baseclass < handle
    % Base class for link adaptation. The class is abstract and cannot
    % be instantiated. 
    %
    % PROPERTIES (protected)
    %
    %   mod_table
    %
    %     current modulations; Nsub x packet_len matrix
    %
    %   code_rate
    %
    %   packet_len
    %
    %     packet length in number of OFDM (or whatever) symbols
    %
    % METHODS
    %
    %   update(INFO)
    %
    %     Process information about latest packet. Updates mod_table,
    %     code_rate, and packet_len.
    %
    %     INFO: struct that can be handed to a feature_baseclass object
    % 
    %   STAT = stats()
    %
    %     Return collected statistics (optional).
    %
    %   next_chan()
    %
    %    Called by run_simulation when after the channel has been reset.
    %
    %   reset()
    %
    %     Reset internal variables to initial state.
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (SetAccess=protected)
        mod_table
        code_rate
        packet_len
    end
    
    methods        
        update(obj,info);
        function stat = stats(obj)
            stat = {};
        end
        next_chan(obj);
        reset(obj)
    end
end
