classdef transceiver_bicmofdm < transceiver_baseclass
    % Transceiver implementing BICM-OFDM with link adaptation and optional
    % random erasures. Implements transceiver_baseclass. The INFO struct
    % provided to the link adaptation contains the fields 'packet_error'
    % (0 or 1), 'snr' (post-equalization signal-to-noise ratio per
    % subcarrier, linear scale), and 'doppler_shift' (in Hz).
    %
    % CONSTRUCTORS
    %
    %   OBJ = transceiver_bicmofdm(FFT_SIZE,CP_SIZE,NPACKETS,ARG1,VAL1,
    %   ...,ARGN,VALN)
    %
    %     FFT_SIZE: number of subcarriers
    %     CPSIZE: length of cyclic prefix [samples]
    %     NPACKETS: number of packets per run of transceive()
    %     Possible ARGS are:
    %     'MAX_PERASURE': probability that packets are lost for unmodeled
    %     causes (default 0)
    %     'NTRAIN': number of OFDM symbols in a packet used for channel
    %     estimation (default 2)
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        fft_size        % size of the fft (= number of subcarriers)
        cp_size         % length of the cyclic prefix
        ntrain          % number of training symbols per packet (for
                        % channel estimation)
        train           % training symbols [a (fft_size-cp_size)xntrain
                        % matrix containing only +1 and -1]
        npackets        % number of packets per simulation run
        max_perasure    % maximum probability of packet erasures []
    end
    
    methods (Hidden)
        function w = ofdm(obj,channel,s,eq)
            s = sqrt(length(s))*ifft(s);        % apply ifft to symbol data
            s = [s(end-obj.cp_size+1:end); s];  % add cyclic prefix
            w = channel.filter(s);              % apply channel
            w = w(obj.cp_size+1:end);           % remove cyclic prefix
            w = fft(w)/sqrt(length(w));         % apply fft, equalize
            w = w.*eq;                          % in frequency domain
        end
    end
    
    methods
        function obj = transceiver_bicmofdm(fft_size,cp_size,...
                npackets,varargin)
            obj.fft_size = fft_size;
            obj.cp_size = cp_size;
            obj.npackets = npackets;
            p = inputParser;
            p.addOptional('max_perasure',0.0,@(x)isnumeric(x)&&...
                isscalar(x)&&(x>=0)&&(x<1));
            p.addOptional('ntrain',2,@(x)isnumeric(x)&&isscalar(x)...
                &&isfinite(x)&&round(x)==x&&x>=2&&x<npackets);
            p.parse(varargin{:});
            obj.max_perasure = p.Results.max_perasure;
            obj.ntrain = p.Results.ntrain;
            obj.train = 2*randi(2,obj.fft_size-obj.cp_size,obj.ntrain)-3;
        end
        
        function [tpt,per] = transceive(obj,channel,amc,la)
            % initialize return values
            tpt = zeros(obj.npackets,1);
            per = zeros(obj.npackets,1);
            
            % choose the packet erasure probability
            perasure = rand()*obj.max_perasure;
            
            % iterate over frames
            for fr=1:obj.npackets
                % compute throughput per sample for this packet, account
                % for training data and cyclic prefix
                ndata = size(la.mod_table,2);
                tpt(fr) = la.code_rate*mean(mean(log2(la.mod_table)))...
                    *ndata/(ndata+obj.ntrain)...
                    *obj.fft_size/(obj.fft_size+obj.cp_size);
                
                % create random data
                [nin,npad] = amc.nbits_in(la.mod_table,la.code_rate);
                inp = randi(2,nin,1)-1;
                
                % encode, pad, interleave, modulate
                x = amc.encmod(inp,la.mod_table,la.code_rate,npad);
                
                % transceive obj.ntrain training symbols for channel
                % estimation
                frp_raw = zeros(obj.fft_size-obj.cp_size,obj.ntrain);
                for i=1:obj.ntrain
                    frp_raw(:,i) = obj.ofdm(channel,obj.train(:,i),...
                        obj.train(:,i));
                end

                % compute noise power
                noise_power = mean(mean(abs(frp_raw(:,2:end)...
                    -frp_raw(:,1:(end-1))).^2));
                
                % find average frequency response, denoise using the
                % assumption that all paths lies within the cyclic prefix
                frp = mean(frp_raw,2);
                tmp = ifft(frp);
                tmp((obj.cp_size+1):end) = 0;
                frp = fft(tmp);
                
                % compute zero-forcing equalizer
                eq = 1./frp;
                
                % compute post-equalization snrs (actually independent of
                % the equalizer because this is siso)
                posteq_snr = abs(frp).^2/noise_power;
                
                % transceive data symbols
                y = zeros(ndata*(obj.fft_size-obj.cp_size),1);
                for sy=1:ndata
                    % index set of this symbol
                    idx = (sy-1)*(obj.fft_size-obj.cp_size)...
                        +1:sy*(obj.fft_size-obj.cp_size);
                    
                    % transceive one ofdm symbol
                    y(idx) = obj.ofdm(channel,x(idx),eq);
                end

                if nin==0 % link adaptation decided not to transmit
                    per(fr) = nan;

                elseif rand()<=perasure % packet is lost due to external
                    % factors (e.g., collisions with other nodes)
                    per(fr) = 1;

                else
                    % demodulate, deinterleave, decode
                    outp = amc.demdec(y,la.mod_table,la.code_rate,nin);
                    
                    % check for errors
                    per(fr) = amc.compare(inp,outp,la.code_rate);
                end
                
                % link adaptation
                info = struct(...
                    'packet_error',per(fr),...
                    'snr',posteq_snr,...
                    'doppler_shift',channel.fd);
                la.update(info);
            end
            
            % scale throughputs to Mbit/s
            tpt = tpt/channel.ts/1e6;
        end
    end
end