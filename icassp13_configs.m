function [configs,params] = icassp13_configs(name,varargin)
% [CONFIGS,PARAMS] = icassp13_configs('simulation')
% [CONFIGS,PARAMS] = icassp13_configs('generate_training_data')
% [CONFIGS,PARAMS] = icassp13_configs('tuning')
%
%   Returns configurations for use with the icassp13_... functions. Bundled
%   here so there is a central place for the simulation parameters.
%
%   CONFIGS: cell array of structs for use with run_simulation(...)
%   PARAMS: struct containing the simulation parameters

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

params = struct();

% sample time of the channel [sec]
params.ts = 4e-6/52;

% max Doppler shift of the channel [Hz]
params.fd_max = 111.5;

% number of subcarriers
params.fft_size = 64;

% length of the cyclic prefix
params.cp_size = 12;

% worst-case path delay
params.max_delay = params.cp_size;

% maximum number of taps
params.max_ntaps = floor(params.max_delay*3/4);

% number of OFDM symbols per frame
params.nsymbols = 25;

% number of frames per channel
params.npackets = 100;

% [min max] average channel (pre-equalization) signal-to-noise ratio [dB]
params.snr = [5 40];

% max probability of erasure
params.max_perasure = 0.3;

% bound on the size of the predictors code books
params.code_book_size = 100;

configs = {};
cnt = 1;
nsubcarriers = params.fft_size-params.cp_size;

% identical for all configs
transceiver = @()transceiver_bicmofdm(params.fft_size,...
    params.cp_size,params.npackets,'max_perasure',params.max_perasure);
amc = @()amc_qamviterbi();
channel = @()channel_mtrayleigh(params.ts,params.fd_max,...
    params.max_ntaps,params.max_delay,'snr',params.snr);
feature1 = @()feature_sortedsnr([5 10 20 40]);
feature2 = @()feature_meansnr();

cnt = 1;
switch name
    case 'simulation'       
        % ARF
        la = la_arf(nsubcarriers);
        configs{cnt} = struct('name','ARF',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;

        % NWM
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_nwm(0.5,0.7,params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature1(),predictors);
        configs{cnt} = struct('name','NWM Sorted SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;
        
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_nwm(2,0.5,params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature2(),predictors);
        configs{cnt} = struct('name','NWM Mean SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;      
        
        % QKLMS
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_qklms(0.2,'kernel_type',...
                'gaussian','kernel_params',2,'epsilon',1,...
                'code_book_size',params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature1(),predictors);
        configs{cnt} = struct('name','QKLMS Sorted SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;        
        
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_qklms(0.2,'kernel_type',...
                'gaussian','kernel_params',6,'epsilon',0.5,...
                'code_book_size',params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature2(),predictors);
        configs{cnt} = struct('name','QKLMS Mean SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;  
        
        % KNN
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_knn(25,0.5,params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature1(),predictors);
        configs{cnt} = struct('name','KNN Sorted SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;
        
        predictors = cell(6,1);
        for mcs=1:6 % coding scheme/packet length
            predictors{mcs} = predictor_knn(25,5,params.code_book_size);
        end
        la = la_max_goodput(nsubcarriers,feature2(),predictors);
        configs{cnt} = struct('name','KNN Mean SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;

    case 'generate_training_data'
        la = la_max_goodput(nsubcarriers,feature1(),{},'mode','tuning',...
            'stats',true);
        configs{cnt} = struct('name','Generate Training Data',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;

    case 'tuning'
        la = la_max_goodput(nsubcarriers,feature1(),{});
        configs{cnt} = struct('name','Tuning Sorted SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;
        la = la_max_goodput(nsubcarriers,feature2(),{});
        configs{cnt} = struct('name','Tuning Mean SNR',...
            'transceiver',transceiver(),'amc',amc(),'la',la,...
            'channel',channel());
        cnt = cnt+1;
        
    otherwise
        error('unknown name');
end
