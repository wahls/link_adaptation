classdef predictor_knn < predictor_baseclass
    % K-nearest-neighbor prediction of packet error rates with density
    % based updating rule as described in the Ph.D. thesis "Machine
    % learning for link adaptation in wireless networks" by R. C. Daniels
    % (http://repositories.lib.utexas.edu/handle/2152/ETD-UT-2011-12-4509).
    % The number of code book entries is kept below a given bound using a
    % density-based neighbor replacement rule. The update method only
    % allows the observations y=0 and y=1. Seperate code books are used for
    % each observation class. Implements predictor_baseclass.
    %
    % CONSTRUCTORS
    %
    %   OBJ = predictor_knn(NNEIGHBORS,RHO_MAX,MAX_DICT_SIZE)
    %
    %     NNEIGHBORS: number of neighbors used for a prediction
    %     RHO_MAX: density bound for deciding whether the oldest among all
    %     code book entries or the oldest neighbor should be replaced with
    %     a new observation (zero=always replace the oldest code book
    %     entry,inf=always replace the oldest neighbor)
    %     MAX_DICT_SIZE: max number of code book entries, a seperate code
    %     book is used for each allowed observation (y=0 or y=1)

    % Copyright 2012 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        nneighbors
        rho_max
        max_dict_size
        X
        t
        cur_t
    end
    
    methods        
        function obj = predictor_knn(nneighbors,rho_max,max_dict_size)
            obj.X = cell(2,1); % {1} -> y==0, {2} -> y==1
            obj.t = cell(2,1);
            obj.cur_t = 0;
            obj.nneighbors = nneighbors;
            obj.rho_max = rho_max;
            obj.max_dict_size = max_dict_size;
        end
        
        function y = predict(obj,x)
            n1 = size(obj.X{1},2);
            n2 = size(obj.X{2},2);
            if n1+n2>0
                [~,ind] = sort(sum(abs([obj.X{1} obj.X{2}] ...
                    -x(:,ones(1,n1+n2))).^2,1),'ascend');
                num = min(n1+n2,obj.nneighbors);
                if num>0
                    tmp = 1/num;
                else
                    tmp = 0;
                end
                ind = ind(1:num);
                y = sum(ind>n1)*tmp;
            else
                y = 0;
            end
        end
        
        function update(obj,x,y)
            [m,n] = size(obj.X{y+1});
            if n==0
                obj.X{y+1} = x;
                obj.t{y+1} = obj.cur_t;
            else
                [foo,ind] = sort(sum(abs(obj.X{y+1}-x(:,ones(1,n))).^2,1), ...
                    'ascend');
                num = min(n,obj.nneighbors);
                tmp = max(foo(1:num))^(m/2);
                if tmp>0
                    tmp = 1/tmp;
                end
                rho = num*tmp;
                if rho<=obj.rho_max
                    ind = 1:n;
                else
                    ind = ind(1:num);
                end
                if n==obj.max_dict_size||rho>obj.rho_max
                    [~,i] = min(obj.t{y+1}(ind));
                    obj.X{y+1}(:,ind(i)) = x;
                    obj.t{y+1}(ind(i)) = obj.cur_t;
                else
                    obj.X{y+1} = [obj.X{y+1} x];
                    obj.t{y+1} = [obj.t{y+1} obj.cur_t];
                end
            end
            obj.cur_t = obj.cur_t+1;
        end
        
        function reset(obj)
            obj.X = cell(2,1);
            obj.t = cell(2,1);
            obj.cur_t = 0;
        end
    end
end