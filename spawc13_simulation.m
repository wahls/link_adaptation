function spawc13_simulation(nruns,nchan)
% spawc13_simulation(NRUNS,NCHANS)
%
%   Performs the actual simulation described in the paper. Results are
%   saved to files. Requires Parallel Computing Toolbox.
%
%   NRUNS: number of simulation runs to perform
%   NCHAN: number of channels per simulation run.

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% console output will be saved to a file for later reference
ds = datestr(now);
if ~isdir('results-spawc13')
    mkdir('results-spawc13');
end
file_name = sprintf('./results-spawc13/%s_sim',ds);
diary([file_name,'.txt']);

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%   small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

% switch to parfor-compatible random number generator
stream = RandStream('mrg32k3a');
RandStream.setGlobalStream(stream);

% show configuration details
[configs,params] = spawc13_configs('simulation');
nruns
nchan
params

%%% Run nruns simulations %%%

gpts = [];
pers = [];
for k=1:ceil(nruns/parfor_max)
    parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
        fprintf('[%s] simulation %i has been started\n',datestr(now),i);
        set(stream,'Substream',i);
        [g,p,~] = run_simulation(i,nchan,configs);
        g(isnan(g)) = 0;
        gpts = [gpts;g];
        p(isnan(p)) = 1;
        pers = [pers;p];
        fprintf('[%s] simulation %i has been completed\n',datestr(now),i);
    end
    diary off; % flush diary
    diary on;
end

%%% Generate plots, save and display results %%%

font_size = 18;
line_width = 2;

fig1 = figure('Visible','Off');
hold all;
names = {};
for i=1:size(gpts,2)
    h = cdfplot(gpts(:,i));
    set(h,'LineWidth',line_width);
    names{i} = configs{i}.name;
end
hold off;
set(gca,'FontSize',font_size,'LooseInset',get(gca,'TightInset'));
legend(names,'Location','SouthEast');
title('');
xlabel('x [MBits/s]')
ylabel('Occurence of Goodput <= x [%]');
print(fig1,'-depsc',sprintf('%s-goodput.eps',file_name));

fig2 = figure('Visible','Off');
hold all;
for i=1:size(pers,2)
    h = cdfplot(pers(:,i));
    set(h,'LineWidth',line_width);
end
hold off;
set(gca,'FontSize',font_size,'LooseInset',get(gca,'TightInset'));
legend(names,'Location','SouthEast');
title('');
xlabel('x [Errors/Packet]')
ylabel('Occurence of PER <= x [%]');
print(fig2,'-depsc',sprintf('%s-per.eps',file_name));

save(sprintf('%s.mat',file_name),'nchan','nruns','params','gpts','pers',...
    'fig1','fig2');
fprintf('\nSaved results in "%s*.{txt,mat,eps}."\n',file_name);

% display/plot results

goodputs = mean(gpts)
pers = mean(pers)
diary off;
set(fig1,'Visible','On');
set(fig2,'Visible','On');

