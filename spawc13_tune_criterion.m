function spawc13_tune_criterion(nruns,nchan)
% spawc13_tune_criterion(NRUNS,NCHAN)
%
%   Brute force search for good criterion parameters. Results are
%   saved to files. Requires Parallel Computing Toolbox.
%
%   NRUNS: number of simulation runs to perform
%   NCHAN: number of channels per simulation run.

% Copyright 2013 Sander Wahls
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%%% Setup %%%

% console output will be saved to a file for later reference
ds = datestr(now);
if ~isdir('results-spawc13')
    mkdir('results-spawc13');
end
file_name = sprintf('./results-spawc13/%s_tune_criterion',ds);
diary([file_name,'.txt']);

% check if there is a pool of MATLAB workers available
parfor_max = matlabpool('size'); % maximum length of parfor loops (keep
%     small to avoid out of memory errors)
if parfor_max==0
    error('run matlabpool first');
end

% show configuration details
fprintf('\nSetup:\n');
nruns
nchan
[~,params] = spawc13_configs('tune_criterion',0,0);
params
fprintf('\n');

% switch to parfor-compatible random number generator
stream = RandStream('mrg32k3a');
RandStream.setGlobalStream(stream);

%%% Run nruns simulations %%%

fprintf('\n\nWill evaluate baseline algorithms.\n\n');
configs = spawc13_configs('baseline');
gpts = [];
fprintf('[%s] ARF ... ',datestr(now));
for k=1:ceil(nruns/parfor_max)
    parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
        set(stream,'Substream',i);
        [g,~,~] = run_simulation(i,nchan,configs);
        gpts = [gpts g(~isnan(g))];
    end
end
gpts'
mean_gpt = mean(mean(gpts))

%%% Perform grid search for good parameters %%%

fprintf('\n\nWill perform grid search in order to find a good\n');
fprintf('exporation radius rho and good weight eta.\n\n');
etas = [0.1 0.2 0.3 0.4];
rhos = [0 0.05 0.1 0.2 0.3];
best_rho = -1;
best_eta = -1;
best_gpt = -1;
for rho=rhos
    for eta=etas
        fprintf('[%s] evaluating eta=%g and rho=%g ... ',datestr(now),...
            eta,rho);
        % compute mean goodput over nruns simulation runs
        configs = spawc13_configs('tune_criterion',eta,rho);
        gpts = zeros(nruns,1);
        for k=1:ceil(nruns/parfor_max)
            parfor i=((k-1)*parfor_max+1):min(k*parfor_max,nruns)
                set(stream,'Substream',i);
                [g,~,~] = run_simulation(i,nchan,configs);
                gpts(i) = mean(g(~isnan(g)));
            end
        end
        gpt = mean(gpts);
        if gpt>best_gpt
            best_gpt = gpt;
            best_eta = eta;
	    best_rho = rho;
        end
        fprintf('%g\n',gpt);
        diary off; % flush diary
        diary on;        
    end
end
fprintf('\n  => best_rho=%g, best_eta=%g, best_gpt=%g',best_rho,...
    best_eta,best_gpt);

% stop writing console output to file
diary off;
save(sprintf('%s.mat',filename),'nchan','nruns','params','g','p',...
    'fig1','fig2');
fprintf('\nResults have been saved in the files ''%s.{txt,mat,eps}''.\n',...
    file_name);
