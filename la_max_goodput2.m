classdef la_max_goodput2 < la_baseclass
    % Frequency-selective link adaptation aiming at goodput maximization.
    % Implements la_baseclass. Modulations are chosen using SNR tresholds
    % which are offset dynamically. The best code rate and offset are
    % chosen with respect to the criterion
    %
    %  J = goodput/(1-eta*(last_offset-new_offset+rho)^2).
    %
    % Predictors are used to learn the mapping between some features and
    % the packet error rates so that the goodput can actually be predicted.
    % Update(...) adds a field 'delta' containing the current offset in dB
    % before calling the feature extraction that generates the features for
    % the predictors.
    %
    % CONSTRUCTORS
    %
    %   OBJ = la_max_goodput2(NSUBCARRIERS,FEATURE,PREDICTORS,ARG1,VAL1,
    %   ...,ARGN,VALN)
    %
    %     NSUBCARRIERS: the number of subcarriers
    %     FEATURE: feature_baseclass object
    %     PREDICTORS: cell array of predictor_baseclass objects (one
    %     per code rate)
    %     Possible ARGS are
    %     'MODS': vector of available modulations (default [2 4 16 64])
    %     'CODE_RATES': vector of available code rates (default [0.5 0.75])
    %     'PACKET_LENS': vector of packet lengths per code rate (code rates
    %     may repeat, default is [25 25])
    %     'THRESHOLDS': matrix of SNR thresholds per code rate and
    %     modulation (default [0 5 13 19;4 9 16 23]')
    %     'RHO': exploration horizon for the criterion (default 0)
    %     'ETA': weight for the criterion (default 0)
    %     'MODE': 'default' or 'tuning'; 'tuning' selects thresholds and
    %     code rates randomly, use with 'STATS' to obtain training data
    %     for predictors (default 'default')
    %     'STATS': keep statistics, calling obj.stats will return a cell
    %     array of structs that can be evaluated with test_predictors(...)
    %
    % METHODS
    %
    %   R = test_predictors(STATS,PREDICTORS)
    %
    %     Test prediction accuracy w.r.t. packet error rates.
    %
    %     STATS: output of obj.stats() after a simulation has been run
    %     in 'tuning' mode and with 'STATS'=true
    %     R: vector of length(stats), R(i)=1 if round(predicted PER)=
    %     stats{i}.info.packet_error and 0 otherwise 
    
    % Copyright 2013 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        feature             % feature_baseclass object
        predictors          % cell array of predictor_baseclass objects
        nsubcarriers
        mods
        code_rates
        packet_lens
        thresholds
        deltas
        delta_indices
        rho                 % exploration radius
        eta                 % weight
        nmods
        cspl
        ncspl
        keep_stats
        stat
        mode
    end
    
    methods (Hidden)
        function d = delta(obj,cspl)
            d = obj.deltas(obj.delta_indices(cspl));
        end
        
        function tpt = throughput(obj,snrs_dB,cspl,delta_index)
            cspl_backup = obj.cspl;
            delta_indices_backup = obj.delta_indices;
            mod_table_backup = obj.mod_table;
            obj.set_mods(snrs_dB,cspl,delta_index);
            tpt = sum(sum(log2(obj.mod_table)));
            obj.cspl = cspl_backup;
            obj.delta_indices = delta_indices_backup;
            obj.mod_table = mod_table_backup;
        end
        
        function set_mods(obj,snrs_dB,cspl,delta_index)
            obj.cspl = cspl;
            obj.delta_indices(cspl) = delta_index;
            obj.mod_table = ones(obj.nsubcarriers,1);
            delta = obj.delta(cspl);
            ind = snrs_dB>=obj.thresholds(obj.nmods,cspl)+delta;
            obj.mod_table(ind) = obj.mods(obj.nmods);
            for j=1:obj.nmods-1
                ind = snrs_dB>=obj.thresholds(j,cspl)+delta&...
                    snrs_dB<obj.thresholds(j+1,cspl)+delta;
                obj.mod_table(ind) = obj.mods(j);
            end
            obj.mod_table = obj.mod_table*ones(1,obj.packet_len);
        end
        
        function set_common_mcs(obj)
            obj.cspl = obj.ncspl;
            obj.mod_table = obj.mods(obj.nmods)*ones(obj.nsubcarriers, ...
                obj.packet_lens(obj.ncspl));
            obj.code_rate = obj.code_rates(obj.ncspl);
            obj.packet_len = obj.packet_lens(obj.ncspl);
        end
        
        function J = criterion(obj,per,tpt,cspl,delta)
            J = (1-per)*tpt/(1+obj.eta*(obj.delta(cspl)-delta-obj.rho)^2);
        end
    end
    
    methods
        function obj = la_max_goodput2(nsubcarriers,feature,...
                predictors,varargin)
            obj.nsubcarriers = nsubcarriers;
            obj.feature = feature;
            obj.predictors = predictors;
            p = inputParser;
            p.addOptional('mods',[2 4 16 64],@(x)isnumeric(x)...
                &&isvector(x)&&sum(x<=0|x==inf)==0&&length(x)>1);
            p.addOptional('code_rates',[0.5 0.75],@(x)isnumeric(x)...
                &&isvector(x)&&sum(x<0|x>1)==0);
            p.addOptional('packet_lens',[25 25],@(x)isnumeric(x)...
                &&isvector(x)&&sum(x<1|x==inf)==0);
            p.addOptional('thresholds',...
                [0 5 13 19;4 9 16 23]',...
                @(x)isnumeric(x)&&ismatrix(x)&&sum(sum(~isfinite(x))==0));
            p.addOptional('deltas',-10:10,@(x)isnumeric(x)...
                &&isvector(x)&&length(x)>1&&sum(~isfinite(x))==0);
            p.addOptional('rho',0,@(x)isnumeric(x)...
                &&isscalar(x)&&isfinite(x)&&x>=0);
            p.addOptional('eta',0,@(x)isnumeric(x)...
                &&isscalar(x)&&isfinite(x)&&x>=0);
            p.addOptional('mode','default',@(x)isvector(x)&&ischar(x)...
                &&(strcmp(x,'default')|strcmp(x,'tuning')));
            p.addOptional('stats',false,@(x)islogical(x));
            p.parse(varargin{:});
            obj.mods = p.Results.mods;
            obj.code_rates = p.Results.code_rates;
            obj.packet_lens = p.Results.packet_lens;
            obj.ncspl = length(obj.code_rates);
            obj.thresholds = p.Results.thresholds;
            obj.deltas = p.Results.deltas;
            obj.delta_indices = ones(obj.ncspl,1);
            obj.rho = p.Results.rho;
            obj.eta = p.Results.eta;
            obj.keep_stats = p.Results.stats;
            obj.stat = {};
            obj.mode = p.Results.mode;
            if length(obj.packet_lens)~=obj.ncspl
                error('code_rates and packet_lens have different lengths');
            end
            obj.nmods = length(obj.mods);
            if sum(size(obj.thresholds)~=[obj.nmods,obj.ncspl])~=0
                error(['size of thresholds is not ',...
                    'length(mods)xlength(code_rates)']);
            end
            obj.set_common_mcs();
        end
        
        function update(obj,info)
            if obj.keep_stats
                info.delta = obj.delta(obj.cspl);
                obj.stat{end+1} = struct('info',info,...
                    'cspl',obj.cspl,'thresholds',obj.thresholds);
            end
            snrs_dB = 10*log10(info.snr);
            switch obj.mode
                case 'default'
                    y = info.packet_error;
                    if isnan(y) % treat no packet as packet error
                        y = 1;
                    end
                    if all(y~=[0 1])
                        error(sprintf('per=%g is not in {0,1,nan}',y));
                    end
                    % find new coding scheme+packet length with highest
                    % predicted criterion
                    best_cspl = -1;
                    best_delta_index = -1;
                    best_J = -inf;
                    for i=1:obj.ncspl
                        for j=1:length(obj.deltas)
                            info.delta = obj.deltas(j);
                            x = obj.feature.extract(info);
                            per = obj.predictors{i}.predict(x);
                            tpt = obj.throughput(snrs_dB,i,j);
                            J = obj.criterion(per,tpt,i,obj.deltas(j));
                            if J>=best_J
                                best_cspl = i;
                                best_delta_index = j;
                                best_J = J;
                                best_per = per;
                            end
                        end
                    end
                    % update thresholds of current coding scheme
                    info.delta = obj.delta(obj.cspl);
                    x = obj.feature.extract(info);
                    obj.predictors{obj.cspl}.update(x,y);
                                        
                case 'tuning'
                    best_cspl = randi(obj.ncspl);
                    best_delta_index = randi(length(obj.deltas));
                    
                otherwise
                    error('unknown mode');
            end
            obj.cspl = best_cspl;
            obj.code_rate = obj.code_rates(best_cspl);
            obj.packet_len = obj.packet_lens(best_cspl);
            obj.delta_indices(best_cspl) = best_delta_index;
            obj.set_mods(snrs_dB,best_cspl,best_delta_index);
        end
        
        function r = test_predictors(obj,stats,predictors)
            r = zeros(1,length(stats));
            for i=1:length(r)
                x = obj.feature.extract(stats{i}.info);
                y = stats{i}.info.packet_error;
                if isnan(y) % treat no transmission as packet error
                    y = 1;
                end
                per = predictors{stats{i}.cspl}.predict(x);
                r(i) = round(per)==y;
                predictors{stats{i}.cspl}.update(x,y);
            end
        end
        
        function next_chan(obj)
            % no operation
        end
        
        function stat = stats(obj)
            stat = obj.stat(2:end);
        end
        
        function reset(obj)
            for i=1:length(obj.predictors)
                obj.predictors{i}.reset();
            end
            obj.delta_indices = ones(obj.ncspl,1);
            obj.stat = {};
        end
        
    end
    
end
