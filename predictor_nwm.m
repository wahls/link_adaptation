classdef predictor_nwm < predictor_baseclass
    % Nadaraya-Watson with Merging. Keeps the code book size below a given
    % bound. New observations are merged with the closed existing code book
    % entry once the code book is full. Inspired by F. M. Pouzols and
    % A. Lendasse, “Adaptive kernel smoothing regression for
    % spatio-temporal environmental datasets,” Neurocomput., Aug. 2012.
    % Implements predictor_baseclass.
    %
    % CONSTRUCTORS
    %
    %  OBJ = predictor_nwm(H,DELTA,MAX_DICT_SIZE)
    %
    %    H: kernel bandwidth (a Gaussian kernel is used)
    %    DELTA: governs the merging (zero: replace closed existing code
    %    book entry with new observation; one: ignore new observation)
    %    MAX_DICT_SIZE: bound on the code book size; one it is reached,
    %    new observations will be merged into the code book.

    % Copyright 2012 Sander Wahls
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %    http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.
    
    properties (Hidden)
        X
        k
        h
        delta
        max_dict_size
    end
    
    methods
        function obj = predictor_nwm(h,delta,max_dict_size)
            obj.h = h;
            obj.delta = delta;
            obj.max_dict_size = max_dict_size;
            obj.X = [];
            obj.k = [];
        end
        
        function y = predict(obj,x)
            [m,n] = size(obj.X);
            if n>0
                tmp = exp(-0.5*sum(abs(obj.X-x(:,ones(1,n))).^2,1)/obj.h^2);
                num = sum((obj.k).*(tmp'));
                den = sum(tmp);
            else
                num = 0;
                den = 1;
            end
            if den<=1e-6*num
                y = 0;
            else
                y = num/den;
            end
        end
        
        function update(obj,x,y)
            [m,n] = size(obj.X);
            if n>=obj.max_dict_size
                [v,i] = min(sum(abs(obj.X-x(:,ones(1,n))).^2));
                obj.X(:,i) = obj.delta*obj.X(:,i)+(1-obj.delta)*x;
                obj.k(i) = obj.delta*obj.k(i)+(1-obj.delta)*y;
            else
                obj.X = [obj.X x];
                obj.k = [obj.k;y];
            end
        end
        
        function reset(obj)
            obj.X = [];
            obj.k = [];
        end
    end
end